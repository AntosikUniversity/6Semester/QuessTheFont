﻿### Description (RU)
#### «Угадай шрифт»
###### Баллов за задание - 25. 

Разработать интерактивную игру «Угадай шрифт». 

В оконном приложении выводится строка случайным шрифтом (все параметры шрифта определяются случайным образом). 

Целью пользователя за наименьшее число шагов определить все параметры шрифта: название шрифта, размер, тип начертания, цвет шрифта.  
При этом у пользователя есть подсказки: 
* можно посмотреть случайную букву имени шрифта; 
* посмотреть начертание своей фразы одним выбранным шрифтом; 
* узнать больше или меньше искомого предполагаемый размер шрифта; 
* выполнить не более 4 проверок цвета по его трем составляющим. 

Ваша программа должна вести лог-файл действий пользователя, а также хранить информацию о 10 пользователях, которые выполнили задание лучше всех.  

Так же необходимо разработать систему очков, которые будут начисляться и сниматься с пользователя за каждый его ход.  

Постарайтесь разработать как можно более приятный интерфейс для данной игры. (Например, недопустимо просить пользователя вводить всю информацию в Textbox). 