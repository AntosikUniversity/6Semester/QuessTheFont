﻿namespace QuessTheFont.Helpers
{
    public static class Enums
    {
        public static string[] FontWeight = {"Light", "Normal", "Medium", "Bold"};
        public static string[] FontStyle = {"Normal", "Oblique", "Italic"};
    }
}