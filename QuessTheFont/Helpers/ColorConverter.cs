﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace QuessTheFont.Helpers
{
    public class ColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(values[0] is double r)) return null;
            if (!(values[1] is double g)) return null;
            if (!(values[2] is double b)) return null;
            return new SolidColorBrush(Color.FromRgb((byte) r, (byte) g, (byte) b));
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (!(value is SolidColorBrush brush)) return new object[] {0, 0, 0};

            var color = brush.Color;

            return new object[] {color.R, color.G, color.B};
        }
    }
}