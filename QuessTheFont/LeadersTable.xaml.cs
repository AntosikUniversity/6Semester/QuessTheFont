﻿using System.Collections.Generic;
using System.Windows;
using QuessTheFont.ViewModels;

namespace QuessTheFont
{
    /// <summary>
    ///     Interaction logic for LeadersTable.xaml
    /// </summary>
    public partial class LeadersTable : Window
    {
        public LeadersTable(List<UserViewModel> leaders)
        {
            InitializeComponent();
            DataContext = leaders;
        }
    }
}