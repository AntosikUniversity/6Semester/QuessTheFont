﻿namespace QuessTheFont.Interfaces
{
    public interface IUser
    {
        string Username { get; set; }
        int Points { get; set; }
    }
}