﻿using System.Windows.Media;

namespace QuessTheFont.Interfaces
{
    public interface IFont
    {
        string FontFamily { get; set; }
        int FontSize { get; set; }
        string FontStyle { get; set; }
        string FontWeight { get; set; }
        SolidColorBrush FontColor { get; set; }
    }
}