﻿using System;
using System.Xml.Serialization;
using QuessTheFont.Interfaces;

namespace QuessTheFont.ViewModels
{
    [Serializable]
    [XmlType("Player")]
    public class UserViewModel : ViewModelBase, IUser
    {
        private int _points;
        private string _username;

        public UserViewModel()
        {
            Username = "Гость";
            Points = 0;
        }

        public UserViewModel(string username)
        {
            Username = username;
            Points = 0;
        }

        public string Username
        {
            get => _username;
            set => Change(ref _username, value);
        }

        public int Points
        {
            get => _points;
            set => Change(ref _points, value);
        }
    }
}