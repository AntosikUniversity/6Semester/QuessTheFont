﻿using System;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Media;
using QuessTheFont.Helpers;
using QuessTheFont.Interfaces;

namespace QuessTheFont.ViewModels
{
    public class FontViewModel : ViewModelBase, IFont
    {
        public const int MAX_FONT_SIZE = 72;
        public const int MIN_FONT_SIZE = 8;

        public static Random Random = new Random();

        public static string[] FontsFamilies =
            new InstalledFontCollection().Families.Select(font => font.Name).ToArray();

        private SolidColorBrush _fontColor;
        private string _fontFamily;
        private int _fontSize;
        private string _fontStyle;
        private string _fontWeight;

        public string FontFamily
        {
            get => _fontFamily;
            set => Change(ref _fontFamily, value);
        }

        public int FontSize
        {
            get => _fontSize;
            set => Change(ref _fontSize, value);
        }

        public string FontStyle
        {
            get => _fontStyle;
            set => Change(ref _fontStyle, value);
        }

        public string FontWeight
        {
            get => _fontWeight;
            set => Change(ref _fontWeight, value);
        }

        public SolidColorBrush FontColor
        {
            get => _fontColor;
            set => Change(ref _fontColor, value);
        }

        public static FontViewModel CreateRandomFont()
        {
            var randomFont = FontsFamilies[Random.Next(0, FontsFamilies.Length)];
            var randomColor = Color.FromRgb(
                (byte) Random.Next(0, 255),
                (byte) Random.Next(0, 255),
                (byte) Random.Next(0, 255)
            );
            var randomColorBrush = new SolidColorBrush(randomColor);
            var randomSize = Random.Next(MIN_FONT_SIZE, MAX_FONT_SIZE);
            var randomWeight = Enums.FontWeight[Random.Next(0, Enums.FontWeight.Length)];
            var randomStyle = Enums.FontStyle[Random.Next(0, Enums.FontStyle.Length)];

            return new FontViewModel
            {
                FontFamily = randomFont,
                FontColor = randomColorBrush,
                FontSize = randomSize,
                FontWeight = randomWeight,
                FontStyle = randomStyle
            };
        }
    }
}