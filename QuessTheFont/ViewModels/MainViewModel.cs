﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.VisualBasic;
using QuessTheFont.Helpers;

namespace QuessTheFont.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private const string LeadersFile = "leaders.xml";
        private const int FontFamilyPoints = 50000;
        private const int FontSizePoints = 25000;
        private const int FontStylePoints = 10000;
        private const int FontWeightPoints = 10000;
        private const int FontColorPoints = 50000;

        private UserViewModel _currentUser;
        private FontViewModel _fontAnswer;
        private FontViewModel _fontQuestion;
        private List<UserViewModel> _leaders;
        private string _log;

        public MainViewModel()
        {
            var username = Interaction.InputBox("Введите имя игрока", "Вход в игру", "Гость");
            User = new UserViewModel(username);

            Surrend = new Command(SurrendGame);
            NewGame = new Command(StartNewGame);
            CheckAnswer = new Command(CheckCurrentAnswer);
            ChangeUser = new Command(ChangeGameUser);
            ShowLeadersTable = new Command(ShowLeadersTableWindow);

            GetFontFamilyTip = new Command(FontFamilyTip);
            GetFontSizeTip = new Command(FontSizeTip);
            GetFontColorTip = new Command(FontColorTip);
            GetTextTip = new Command(TextTip);

            Leaders = LoadLeadersList();

            StartNewGame();
        }

        public ICommand Surrend { get; }
        public ICommand NewGame { get; }
        public ICommand CheckAnswer { get; }
        public ICommand ChangeUser { get; }
        public ICommand ShowLeadersTable { get; }

        public UserViewModel User
        {
            get => _currentUser;
            set => Change(ref _currentUser, value);
        }

        public FontViewModel Font
        {
            get => _fontQuestion;
            set => Change(ref _fontQuestion, value);
        }

        public FontViewModel Answer
        {
            get => _fontAnswer;
            set => Change(ref _fontAnswer, value);
        }

        public string Log
        {
            get => _log;
            set => Change(ref _log, value);
        }

        public List<UserViewModel> Leaders
        {
            get => _leaders;
            set => Change(ref _leaders, value);
        }

        private void ChangeGameUser()
        {
            var username = Interaction.InputBox("Введите имя игрока", "Сменить пользователя", "Гость");

            User = new UserViewModel(username);

            StartNewGame();
        }

        #region Leaders

        private List<UserViewModel> LoadLeadersList()
        {
            var list = SerializeHelpers.DeSerializeObject<List<UserViewModel>>(LeadersFile);

            return list ?? new List<UserViewModel>();
        }

        private void SaveLeadersList(UserViewModel currentUser)
        {
            var index = Leaders.FindIndex(leader => leader.Points < currentUser.Points);

            Leaders.Add(currentUser);
            Leaders = Leaders.Take(10).ToList();

            SerializeHelpers.SerializeObject(Leaders, LeadersFile);
        }

        private void ShowLeadersTableWindow()
        {
            new LeadersTable(Leaders).ShowDialog();
        }

        #endregion

        #region Game

        public bool IsFontFamilyValid => Font.FontFamily == Answer?.FontFamily;
        public bool IsFontColorValid => Font.FontColor.Color == Answer?.FontColor?.Color;
        public bool IsFontStyleValid => Font.FontStyle == Answer?.FontStyle;
        public bool IsFontWeightValid => Font.FontWeight == Answer?.FontWeight;
        public bool IsFontSizeValid => Equals(Font.FontSize, Answer?.FontSize);

        private void StartNewGame()
        {
            Font = FontViewModel.CreateRandomFont();
            Answer = new FontViewModel();

            UpdateProps();
            ClearTips();
            User.Points = 0;

            Log += $"Новая игра! В игре: {User.Username}\n";
        }

        private void SurrendGame()
        {
            var color = Font.FontColor.Color;
            var colorStr = $"({color.R},{color.G},{color.B})";
            MessageBox.Show($"Загаданный шрифт:\n\n" +
                            $"Название: {Font.FontFamily}\n" +
                            $"Цвет: {colorStr}\n" +
                            $"Ширина: {Font.FontWeight}\n" +
                            $"Стиль: {Font.FontStyle}\n" +
                            $"Размер: {Font.FontSize}");

            Log += $"Игрок {User.Username} сдался.\n";
            StartNewGame();
        }

        private void UpdateProps()
        {
            OnPropertyChanged(nameof(IsFontFamilyValid));
            OnPropertyChanged(nameof(IsFontColorValid));
            OnPropertyChanged(nameof(IsFontStyleValid));
            OnPropertyChanged(nameof(IsFontWeightValid));
            OnPropertyChanged(nameof(IsFontSizeValid));
        }

        private void CheckCurrentAnswer(object objBrush)
        {
            if (objBrush is SolidColorBrush brush)
                Answer.FontColor = brush;

            UpdateProps();
            User.Points = RecalculatePoints();

            Log += $"Попытка угадать шрифт! Посмотрим...\n";
            Log += IsFontFamilyValid ? $"Название шрифта ...верно!\n" : $"Название шрифта не верно :с\n";
            Log += IsFontSizeValid ? $"Размер шрифта ...верен!\n" : $"Размер шрифта не верен :с\n";
            Log += IsFontStyleValid ? $"Стиль шрифта ...верен!\n" : $"Стиль шрифта не верен :с\n";
            Log += IsFontWeightValid ? $"Толщина шрифта ...верна!\n" : $"Толщина шрифта не верна :с\n";
            Log += IsFontColorValid ? $"Цвет шрифта ...верен!\n" : $"Цвет шрифта не верен :с\n";

            Log += "\n";

            if (IsFontColorValid && IsFontSizeValid && IsFontFamilyValid && IsFontStyleValid && IsFontWeightValid)
            {
                MessageBox.Show("Поздравляем! Вам удалось отгадать шрифт!");
                Log += $"Игрок {User.Username} победил!\n";
                SaveLeadersList(User);
                StartNewGame();
            }
        }

        private int RecalculatePoints()
        {
            var points = 0;

            if (IsFontFamilyValid)
            {
                double familyMultiplier = IsFontFamilyTip ? (Font.FontFamily.Length - 1) / Font.FontFamily.Length : 1;

                points += Convert.ToInt32(FontFamilyPoints * familyMultiplier);
            }

            if (IsFontSizeValid)
            {
                var sizeMultiplier = IsFontSizeTip ? 0.75 : 1;

                points += Convert.ToInt32(FontSizePoints * sizeMultiplier);
            }

            if (IsFontStyleValid) points += FontStylePoints;

            if (IsFontWeightValid) points += FontStylePoints;

            if (IsFontColorValid)
            {
                double colorMultiplier = _fontColorTipCount / 4;

                points += Convert.ToInt32(FontStylePoints * colorMultiplier);
            }

            return points;
        }

        #endregion

        #region Tips

        public bool IsFontFamilyTip { get; private set; } = true;
        public bool IsFontSizeTip { get; private set; } = true;
        public bool IsFontColorTip { get; private set; } = true;
        public bool IsTextTip { get; private set; } = true;

        public string TipFontFamily { get; private set; } = string.Empty;
        public string TipFontSize { get; private set; } = string.Empty;
        public string TipFontColor { get; private set; } = string.Empty;
        public string TipTextFontFamily { get; private set; } = string.Empty;

        public ICommand GetFontFamilyTip { get; }
        public ICommand GetFontSizeTip { get; }
        public ICommand GetFontColorTip { get; }
        public ICommand GetTextTip { get; }

        private byte _fontColorTipCount = 4;

        private void FontFamilyTip()
        {
            IsFontFamilyTip = false;
            TipFontFamily = Font.FontFamily[FontViewModel.Random.Next(0, Font.FontFamily.Length)].ToString();

            OnPropertyChanged(nameof(IsFontFamilyTip));
            OnPropertyChanged(nameof(TipFontFamily));

            Log += $"Игрок {User.Username} использовал подсказку (посмотреть случайную букву имени шрифта).\n";
        }

        private void FontSizeTip(object sizeObj)
        {
            if (!(sizeObj is double size)) return;

            IsFontSizeTip = false;
            if (size > Font.FontSize) TipFontSize = "<";
            else if (size < Font.FontSize)
                TipFontSize = ">";
            else
                TipFontSize = "=";

            OnPropertyChanged(nameof(IsFontSizeTip));
            OnPropertyChanged(nameof(TipFontSize));

            Log += $"Игрок {User.Username} использовал подсказку (больше или меньше размер шрифта).\n";
        }

        private void FontColorTip(object objBrush)
        {
            if (!(objBrush is SolidColorBrush brush)) return;

            var rDiff = Math.Abs(brush.Color.R - Font.FontColor.Color.R);
            var gDiff = Math.Abs(brush.Color.G - Font.FontColor.Color.G);
            var bDiff = Math.Abs(brush.Color.B - Font.FontColor.Color.B);

            var overallDiff = rDiff + gDiff + bDiff;

            TipFontColor = $"Отличие цвета = {overallDiff}";
            OnPropertyChanged(nameof(TipFontColor));

            _fontColorTipCount--;

            Log += $"Игрок {User.Username} использовал подсказку (проверка цвета). Осталось {_fontColorTipCount}\n";

            if (_fontColorTipCount == 0)
            {
                IsFontColorTip = false;
                OnPropertyChanged(nameof(IsFontColorTip));
            }
        }

        private void TextTip(object objFamily)
        {
            if (!(objFamily is string fontFamily)) return;

            IsTextTip = false;
            TipTextFontFamily = fontFamily;

            OnPropertyChanged(nameof(IsTextTip));
            OnPropertyChanged(nameof(TipTextFontFamily));

            Log += $"Игрок {User.Username} использовал подсказку (посмотреть начертание шрифтом). \n";
        }

        private void ClearTips()
        {
            IsFontFamilyTip = true;
            IsFontSizeTip = true;
            IsFontColorTip = true;
            IsTextTip = true;

            TipFontFamily = string.Empty;
            TipFontSize = string.Empty;
            TipFontColor = string.Empty;
            TipTextFontFamily = string.Empty;

            OnPropertyChanged(nameof(IsFontFamilyTip));
            OnPropertyChanged(nameof(IsFontSizeTip));
            OnPropertyChanged(nameof(IsFontColorTip));
            OnPropertyChanged(nameof(IsTextTip));

            OnPropertyChanged(nameof(TipFontFamily));
            OnPropertyChanged(nameof(TipFontSize));
            OnPropertyChanged(nameof(TipFontColor));
            OnPropertyChanged(nameof(TipTextFontFamily));
        }

        #endregion
    }
}